# Drakkar
> Drakkar é meu navio, um deposito pessoal com todo o meu arsenal para mostrar o progresso, o crescimento e até a te incentivar a aprender mais! 

Você é mais que bem vindo para ver, comentar e dar dicas para melhorar.
Cada Projeto possui o seu próprio **READ_ME.md** com a explicação do mesmo.

<!-- 
:![](/header.png):
![](../header.png)
-->

<br />

## Meta

Gustavo Henrique – gustavo._henrique@hotmail.com - [Linkedin](https://www.linkedin.com/in/gustavo-henrique-664b2698/) / [GitHub](https://www.github.com/) / [GitLab](https://gitlab.com/)

**_Todos_** os códigos estão protegidos por `DIREITOS AUTORAIS` COPYRIGHT &copy; 2018.

### Note
**if you don't understand portuguese, please access the archive README-EN.md and You're welcome!**

<!--
 ______               ______               _                       ____  ____                      _                         _  _ _________ __             ____   ____ _  __       _                _  _  
|_   _ \        _   .' ___  |             / |_                    |_   ||   _|                    (_)                       | || |  _   _  [  |           |_  _| |_  _(_)[  |  _  (_)              | || | 
  | |_) | _   _(_) / .'   \_|__   _  .--.`| |-,--. _   __  .--.     | |__| | .---. _ .--.  _ .--. __  .--. _ __   _  .---.  \_|\_|_/ | | \_|| |--. .---.    \ \   / / __  | | / ] __  _ .--.  .--./\_|\_| 
  |  __'.[ \ [  _  | |   ___[  | | |( (`\]| |`'_\ [ \ [  / .'`\ \   |  __  |/ /__\[ `.-. |[ `/'`\[  / /'`\' [  | | |/ /__\\          | |    | .-. / /__\\    \ \ / / [  | | '' < [  |[ `.-. |/ /'`\;      
 _| |__) |\ '/ (_) \ `.___]  | \_/ |,`'.'.| |// | |\ \/ /| \__. |  _| |  | || \__.,| | | | | |    | | \__/ | | \_/ || \__.,         _| |_   | | | | \__.,     \ ' /   | | | |`\ \ | | | | | |\ \._//      
|_______[\_:  /     `._____.''.__.'_[\__) \__\'-;__/\__/  '.__.'  |____||____'.__.[___||__[___]  [___\__.; | '.__.'_/'.__.'        |_____| [___]|__'.__.'      \_/   [___[__|  \_[___[___||__.',__`       
         \__.'                                                                                           |__]                                                                               ( ( __))      
-->