# Drakkar
> Drakkar is my ship, a personal deposit with all my arsenal to show the progress, the growth and even to encourage you to learn more!

You are welcome to view, comment and give tips to improve all the codes.
Each Project has its own ** READ_ME.md ** with explanation of it.

<!-- 
:![](/header.png):
![](../header.png)
-->

<br />

## Meta

Gustavo Henrique – gustavo._henrique@hotmail.com - [Linkedin](https://www.linkedin.com/in/gustavo-henrique-664b2698/) [GitHub](https://www.github.com/) [GitLab](https://gitlab.com/)

** _ All _ ** codes are protected by 'COPYRIGHT' COPYRIGHT &copy; 2018.