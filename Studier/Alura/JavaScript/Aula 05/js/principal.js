var titulo = document.querySelector(".titulo");
titulo.textContent = "Aparecida Nutricionista"

//aula 03

function calcImc(peso, altura) {

    var imc = peso / (altura * altura);
    return imc.toFixed(2);
}


function calcTab() {

    var pacientes = document.querySelectorAll(".paciente");
    for (var i = 0; i < pacientes.length; i++) {

        var paciente = pacientes[i];

        var tdPeso = paciente.querySelector(".info-peso");
        var peso = tdPeso.textContent;

        var tdAlt = paciente.querySelector(".info-altura");
        var altura = tdAlt.textContent;

        var tdImc = paciente.querySelector(".info-imc");

        var pesoValid = true;
        var AltValid = true;



        if (peso <= 0 || peso >= 500) {

            pesoValid = false;
            tdPeso.textContent = "Peso INVALIDO";
            paciente.classList.add("paciente-invalido");

        }
        if (altura <= 0 || altura >= 3.00) {

            AltValid = false;
            tdAlt.textContent = "altura INVALIDO";
            paciente.classList.add("paciente-invalido");

        }
        if (pesoValid && AltValid) {

            var imc = calcImc(peso, altura);
            tdImc.textContent = imc;

        } else {

            tdImc.textContent = "Altura e/ou peso errado!";

        }

    }
}

calcTab();
