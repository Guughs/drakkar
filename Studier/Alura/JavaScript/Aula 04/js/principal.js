var titulo = document.querySelector(".titulo");
titulo.textContent = "Aparecida Nutricionista"

//aula 03

function calcImc() {

    var pacientes = document.querySelectorAll(".paciente");
    console.log(pacientes);
    for (var i = 0; i < pacientes.length; i++) {

        var paciente = pacientes[i];

        var tdPeso = paciente.querySelector(".info-peso");
        var peso = tdPeso.textContent;

        var tdAlt = paciente.querySelector(".info-altura");
        var altura = tdAlt.textContent;

        var tdImc = paciente.querySelector(".info-imc");

        var pesoValid = true;
        var AltValid = true;



        if (peso <= 0 || peso >= 500) {

            pesoValid = false;
            tdPeso.textContent = "Peso INVALIDO";
            paciente.classList.add("paciente-invalido");

        }
        if (altura <= 0 || altura >= 3.00) {

            AltValid = false;
            tdAlt.textContent = "altura INVALIDO";
            paciente.classList.add("paciente-invalido");

        }
        if (pesoValid && AltValid) {

            var imc = peso / (altura * altura);
            tdImc.textContent = imc.toFixed(2);

        } else {

            tdImc.textContent = "Altura e/ou peso errado!";

        }

    }
}
//aULA 4
calcImc();

var botaoAdd = document.querySelector("#adicionar-paciente");
botaoAdd.addEventListener("click", function (event) {

    event.preventDefault();

    var form = document.querySelector("#form-adiciona");

    var nome = form.nome.value;
    var peso = form.peso.value;
    var altura = form.altura.value;
    var gordura = form.gordura.value;

    var pacienteTr = document.createElement("tr");
    var nomeTd = document.createElement("td");
    var pesoTd = document.createElement("td");
    var alturaTd = document.createElement("td");
    var gorduraTd = document.createElement("td");
    var imcTd = document.createElement("td");

    pacienteTr.classList.add("paciente");
    nomeTd.textContent = nome;
    pesoTd.textContent = peso;
    pesoTd.classList.add("info-peso");
    alturaTd.textContent = altura;
    alturaTd.classList.add("info-altura");
    gorduraTd.textContent = gordura;
    imcTd.classList.add("info-imc");

    pacienteTr.appendChild(nomeTd);
    pacienteTr.appendChild(pesoTd);
    pacienteTr.appendChild(alturaTd);
    pacienteTr.appendChild(gorduraTd);
    pacienteTr.appendChild(imcTd);

    var tabela = document.querySelector("#tabela-pacientes");
    tabela.appendChild(pacienteTr);
    calcImc();

});
