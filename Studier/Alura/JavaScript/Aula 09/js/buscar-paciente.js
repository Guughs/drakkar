var button = document.querySelector('#buscar-pacientes');

button.addEventListener("click", function(){
//https://api-pacientes.herokuapp.com/pacientes

var xhr = new XMLHttpRequest();

xhr.open("GET","https://api-pacientes.herokuapp.com/pacientes");
xhr.addEventListener("load", function(){
    var errorAjax = document.querySelector("#erro-ajax");
    if(xhr.status == 200){

        errorAjax.classList.add("invisivel");

        var response = xhr.responseText;
        response = JSON.parse(response);

        response.forEach( function(paciente) {

            addPacientes(paciente);
        });

    }else{

        console.log(xhr.status);
        console.log(xhr.responseText);
        
        errorAjax.classList.remove("invisivel");
    }
});

xhr.send();
    
});