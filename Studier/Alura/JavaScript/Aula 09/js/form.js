//AULA 4

var botaoAdd = document.querySelector("#adicionar-paciente");

botaoAdd.addEventListener("click", function (event) {

    event.preventDefault();

    var form = document.querySelector("#form-adiciona");

    var paciente = obtemPacienteFormulario(form);

    if (!validaPaciente(paciente)) {

        return;
    }

    if (validaCampos(paciente)) {

        var erros = validaCampos(paciente);
        preencheLista(erros);
        return false;

    }

    addPacientes(paciente);

    form.reset();

    document.querySelector("#lista-erro").innerHTML = ""
});


function obtemPacienteFormulario(form) {

    var paciente = {

        nome: form.nome.value,
        peso: form.peso.value,
        altura: form.altura.value,
        gordura: form.gordura.value,
        imc: calcImc(form.peso.value, form.altura.value)

    }

    return paciente;
}




function montaTabela(paciente) {

    var pacienteTr = document.createElement("tr");
    pacienteTr.classList.add("paciente");


    pacienteTr.appendChild(montaTd(paciente.nome, "info-nome"));
    pacienteTr.appendChild(montaTd(paciente.peso, "info-peso"));
    pacienteTr.appendChild(montaTd(paciente.altura, "info-altura"));
    pacienteTr.appendChild(montaTd(paciente.gordura, "info-gordura"));
    pacienteTr.appendChild(montaTd(paciente.imc, "info-imc"));

    return pacienteTr;

}


function montaTd(dados, classe) {

    var td = document.createElement("td");
    td.textContent = dados;
    td.classList.add(classe);
    return td;
}

function validaPaciente(paciente) {

    var erros = "";
    var log = false;

    if (validaPeso(paciente.peso) && validaAltura(paciente.altura)) {
        document.querySelector("#mensagem-erro").textContent = "";
        return true;

    } else if (!validaPeso(paciente.peso)) {

        erros += "Peso Invalido";
        log = true;

    }
    if (!validaAltura(paciente.altura)) {
        if (log) {
            erros += " e ";
        }
        erros += "Altura Invalida";
    }

    document.querySelector("#mensagem-erro").textContent = erros;
    return false;
}

function validaCampos(paciente) {
    var camposVazios = [];

    if (paciente.nome == "") camposVazios.push("Preencha o nome");
    if (paciente.altura == "") camposVazios.push("Preencha a altura");
    if (paciente.gordura == "") camposVazios.push("Preencha a gordura");
    if (paciente.peso == "") camposVazios.push("Preencha o peso");

    if (camposVazios.length > 0) {

        return camposVazios;

    } else {
        return false;
    }
}

function preencheLista(erros) {

    var ul = document.querySelector("#lista-erro");
    ul.innerHTML = "";

    erros.forEach(function (erro) {

        var li = document.createElement("li");
        li.textContent = erro;
        ul.appendChild(li);

    });


}


function addPacientes(paciente){

    var tabela = document.querySelector("#tabela-pacientes");
    tabela.appendChild(montaTabela(paciente));
}