/* Se os 2 for das linhas 11 e 28
*  forem trocados pelo for convencional for(var i = 0; [...])
*  o erro para de acontecer, porem ambas as versões nesse documento funcionam
*/

var inp = document.querySelector("#filtro");

inp.addEventListener("input", function(){
    
    var search = this.value;

    var pacientes = document.querySelectorAll(".paciente");
    
    if(search.length > 0){
        for ( var i in pacientes){
            
            var paciente = pacientes[i];
            var tdNome = paciente.querySelector(".info-nome");
            var nome = tdNome.textContent;

            var expressao = new RegExp(search, "i");
        
            if(!expressao.test(nome)){
                paciente.classList.add("invisivel");
            }else{
                paciente.classList.remove("invisivel");
            }

            
        }   
    }else{
        for(var i in pacientes){
            var paciente = pacientes[i];
            paciente.classList.remove("invisivel");
        }
    }
});
/*
var campoFiltro = document.querySelector("#filtro");

campoFiltro.addEventListener("input", function(){
    
    var search = this.value;
    var pacientes = document.querySelectorAll(".paciente");

    if (search.length > 0) {

        for (var i = 0; i < pacientes.length; i++) { 

            var paciente = pacientes[i];
            var tdNome = paciente.querySelector(".info-nome");
            var nome = tdNome.textContent;
            
            var expressao = new RegExp(search, "i"); 

            if (!expressao.test(nome)) {

                paciente.classList.add("invisivel");
            } else {

                paciente.classList.remove("invisivel");
            }
        }

    } else {

        for (var i = 0; i < pacientes.length; i++) {
            
            var paciente = pacientes[i];
            paciente.classList.remove("invisivel");
        }

    }
});*/