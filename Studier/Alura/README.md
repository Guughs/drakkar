# Alura
> Alura são os cursos livres que realizei no site da [Alura](http://alura.com.br) e coloquei aqui os exercicios da aula para poder mostrar um pouco mais do que aprendi.

[Alura](http://alura.com.br) é um site de cursos focados em tecnologia que assinei para poder ganhar mais conhecimento!
Estou tendo a oportunidade de aprender ainda mais sobre algumas linguagens de programação que já conhecia e também aprendi novas linguagens e novos metodos como MVC.




## Cursos realizados até então:

- [x] Logica de Programação 1 - (Efetuado somente para reafirmar conhecimento )
- [x] Logica de Programação 2 - (Efetuado somente para reafirmar conhecimento )
- [ ] Javascript - (**Em progresso**)
- [ ] Node.js - (**Aguardando**)
- [ ] Python - (**Aguardando**)
- [ ] R - (**Aguardando**)
		
## Meta

Gustavo Henrique – gustavo._henrique@hotmail.com - [Linkedin](https://www.linkedin.com/in/gustavo-henrique-664b2698/) [GitHub](https://www.github.com/) [GitLab](https://gitlab.com/)

**_Todos_** os códigos estão protegidos por `DIREITOS AUTORAIS` COPYRIGHT &copy; 2018.

### Note
**if you don't understand portuguese, please access the archive READ-EN.md and You're welcome!**

