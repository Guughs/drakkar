# Alura
> Alura are the free courses that I performed on [Alura's website](http://alura.com.br) and I put here the exercises of the class to be able to show a little more than I learned.

[Alura](http://alura.com.br) is a site of courses focused on technology that I signed up to be able to gain more knowledge!
I'm having the opportunity to learn even more about some programming languages I already knew and also learned new languages and new methods like MVC.

## Courses taken so far:

- [x] Programming Logic 1 - (Performed only to reaffirm knowledge) (Portuguese Name: Lógica de Programação 1)
- [x] Programming Logic 2 - (Performed only to reaffirm knowledge) (Portuguese Name: Lógica de Programação 2)
- [ ] Javascript - (** In progress **)
- [ ] Node.js - (** Waiting **)
- [ ] Python - (** Waiting **)
- [ ] R - (** Waiting **)


## Meta

Gustavo Henrique - gustavo._henrique@hotmail.com - [Linkedin] (https://www.linkedin.com/in/gustavo-henrique-664b2698/) / [GitHub] (https://www.github.com/) GitLab] / (https://gitlab.com/)

**_All_** codes are protected by 'COPYRIGHT' &copy; 2018.
